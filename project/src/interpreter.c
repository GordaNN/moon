#include <stdio.h>

#include "lib/moon.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Path to script file was not passed\n");
        return -1;
    }

    printf("%s\n", argv[1]);
    // Moon library function for executing script

    return 0;
}
